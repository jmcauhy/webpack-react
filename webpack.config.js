const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')

module.exports = {
   mode: 'development',
   entry: path.resolve(__dirname, 'src', 'index.js'),
   module: {
      rules: [
         { test: /\.(js|jsx)$/, exclude: /node_modules/, use: 'babel-loader' },
         { test: /\.html$/, use: [{ loader: 'html-loader' }] }
      ],
   },
   plugins: [
      new HtmlWebPackPlugin({
         filename: 'index.html',
         template: path.resolve(__dirname, 'src', 'index.html')
      })
   ],
   output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'public')
   }
}